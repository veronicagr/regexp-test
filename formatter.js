module.exports.valueToAPIFormat = function
    valueToAPIFormat(number) {
    if (!number) {
        return '0';
    }

    const numberWithDot = number.replace(",", ".")
    const numberDecimal = Number.parseFloat(numberWithDot).toFixed(2);

    return numberDecimal;
};
