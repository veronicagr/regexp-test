var mocha = require("mocha");
var chai = require("chai");
var formatter = require("./formatter");
var expect = chai.expect;

// "" -> "0"
// "12" -> "12.00"
// "12.30" ->  "12.30"
// "12.999" -> "13.00"
// "12,30" ->  "12.30"
// "12,999" -> "13.00"


describe("formatter", function () {
  describe("#valueToAPIFormat", function () {
    it("se numero 0 for vazio retornar 0", function () {
      expect(formatter.valueToAPIFormat("")).to.deep.equal('0')
    })

  });
});

describe("formatter", function () {
  describe("#valueToAPIFormat", function () {
    it("se o numero  for 12 retornar 12.00", function () {
      expect(formatter.valueToAPIFormat('12')).to.deep.equal('12.00')
    })

  });
});

describe("formatter", function () {
  describe("#valueToAPIFormat", function () {
    it("se o numero  for 12,00 retornar 12.00", function () {
      expect(formatter.valueToAPIFormat('12,00')).to.deep.equal('12.00')
    })

  });
});